{-# LANGUAGE GADTs #-}
import System.IO
import System.Environment
import Data.List
import Data.Hashable
import Control.DeepSeq
import qualified Data.Sequence as Sequence
import Data.Ord
import Prime

type CountMap = Sequence.Seq Int

data CountMin a where 
  CountMin :: (Eq a, Hashable a) => {
   countmaps :: [(CountMap, Int)], 
   toprecords :: [a],
   maxrecords :: Int
} -> CountMin a

countMapAdd :: Hashable a => Int -> Int -> CountMap -> a -> CountMap
countMapAdd modulo prime cMap value = 
   let ind = mod (prime * (hash value)) modulo
   in Sequence.adjust (+1) ind cMap

countMinAdd :: (Hashable a, Eq a) => Int -> CountMin a -> a -> CountMin a
countMinAdd modulo countMin value =
   let newMaps =  map (\(cMap, prime)->(countMapAdd modulo prime cMap value, prime)) $!! (countmaps countMin)
   in let records = toprecords countMin
   in let newToprecords | elem value records = records
                        | otherwise = take (maxrecords countMin) (sortBy (comparing (Down.(lookupCountMaps modulo newMaps))) (value:records) )
   in CountMin newMaps newToprecords $!! (maxrecords countMin)

lookupCountMap :: Hashable a => Int -> Int -> CountMap -> a -> Int
lookupCountMap modulo prime cMap value =  
   let ind = mod (prime * (hash value)) modulo
   in Sequence.index cMap ind

lookupCountMaps :: Hashable a => Int -> [(CountMap, Int)] -> a -> Int
lookupCountMaps m cMaps v = minimum $ map (\(cMap, p) -> lookupCountMap m p cMap v) $ cMaps

lookupCountMin :: (Eq a, Hashable a) => Int -> CountMin a -> a -> Int
lookupCountMin m cMin v = lookupCountMaps m (countmaps cMin) v

rpad m xs p = ys ++ replicate (m - length ys) p 
    where ys = take m xs

main = getArgs >>= runOn >>= putStrLn
   
runOn :: [String] -> IO String
runOn (file:bins:tables:top:[]) = do
   content <- readFile(file)
   let countMinBins = read(bins) :: Int
   let countMinTables = read(tables) :: Int
   let countMinRecords = read(top) :: Int
   let primes = take countMinTables $ dropWhile (<countMinBins) Prime.primes

   let emptyBins = Sequence.fromList $ take countMinBins (repeat 0)
   let countMinEmpty = CountMin (zip (repeat emptyBins) primes) [] countMinRecords
 
   let countAdd = countMinAdd countMinBins 
   let result = foldl' countAdd countMinEmpty $ words content
   let lookup = lookupCountMin countMinBins result
   let scored = map (\w -> (w, lookup w)) $ toprecords result
   let resultLines = map (\(w, s) -> (rpad 20 w ' ') ++ (show s)) scored 
   return (unlines resultLines)
runOn _ = return "usage: <filename> <bins (Int)> <tables (Int)> <n top words (Int)>"
