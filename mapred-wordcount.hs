import System.IO
import System.Environment
import Data.List
import Data.Ord

rpad m xs p = ys ++ replicate (m - length ys) p 
    where ys = take m xs

main = getArgs >>= runOn >>= putStrLn
   
runOn :: [String] -> IO String
runOn (file:top:[]) = do
   content <- readFile(file)
   let records = read(top) :: Int
   let r = sortBy (comparing (Down . snd)) $ map (\x->(head x, length x)) $ group $ sort $ words content
   let result = take records r
   let resultLines = map (\(w, s) -> (rpad 20 w ' ') ++ (show s)) result
   return (unlines resultLines)
runOn _ = return "usage: <filename> <n top words (Int)>"
